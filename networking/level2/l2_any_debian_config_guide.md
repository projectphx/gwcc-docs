# Configure a L2 node + Nanostation XW Loco Station

>*Note:* This node is built on top of Debian 10 x86

## Overview

This guide will go over configuring a private wireless network on a L2 node on
Debian 10.

## Installation

Run through the general install configurations in the Debian Setup. 

In our configureation, I've elected not to install the desktop environment
(XFCE).

### General setup
- Change root password
- Set Hostname 
- Configure any other necessary components. 

### Temporary Internet connection

When building from scratch, the host will need a connection to the Internet to
download necessary software. You can also use offline media, but this guide
assumes you've elected to use the former method.

#### using wpa_supplicant

Generate wpa_supplicant.conf

```
wpa_passphrase <ssid-name> | /etc/wpa_supplicant/wpa_supplicant.conf
_
```
>In the previous example, I've noted an underscore below the command. When you
enter this command, you'll soon discover STDIN is awating. Rest assured, the
program is running fine, it's just waiting for you to type in the password for
the WPA(2)-PSK. Once you enter it, just press [ENTER] to continue. Doing it this
way prevents your password from being saved to `bash_history`.

next, open `/etc/wpa_supplicant/wpa_supplicant.conf` and insert the following at
the top of the file. 

```
ctrl_interface=/run/wpa_supplicant
update_config=1
```

Finally, we'll build a quick configuration for `wlan0` so that we can easily
bring it up during configuration. It will _not_ be brought up automatically at
boot. 

Open `/etc/network/interfaces.d/wlan0-failover` and add the following:
```
iface wlan0 inet dhcp
```

For more information, view the guide below to configure networking on minimal install:
https://wiki.debian.org/WiFi/HowToUse#wpa_supplicant

#### Configure `apt`

By default, the minimal installation of Debain only includes sources for security
patches. Add the following to /etc/apt/sources.list if not already populated.
```
deb http://deb.debian.org/debian/ buster main contrib non-free
deb-src http://deb.debian.org/debian/ buster main contrib non-free

deb http://security.debian.org/debian-security buster/updates main contrib
non-free
deb-src http://security.debian.org/debian-security buster/updates main contrib
non-free
```

#### Update and Install necessary software
```
$ sudo aptitude update && sudo aptitude upgrade
$ sudo aptitude install -y \
    openssh-server \
    curl \
    sudo \
    gnupg \
    hostapd \
    dnsmasq \
    bridge-utils \
    batctl
```

Install frrouting. For Raspbian based systems, visit https://deb.frrouting.org/
for instructions to get the software. 

## L2 System Configuration

### Interface level configurations

To pain a more clear picture, take a moment to imagine that you just registered
to run a L2 node. The network administrator has allocated you the following:
  - An IP for your Nanostation AP and your l2 node (In reality, a DHCP
    reservation would be configured, but during this experimental setup we've
    chosen to go with static IPs).
  - An OSPF Configuration (Area number to join)
  - And a block of IPs that will be issued to L3 nodes in your area. 

| Assignment      | Value           |
|-----------------|-----------------|
| NSM5            | 10.100.1.11/24  |
| L2 Node         | 10.100.1.10/24  |
| Default Gateway | 10.100.1.1      |
| OSPF Area       | 0               |
| IP Block        | 10.100.2.0/24   |

###### eth0
Set up the AP with the assigned IP and then plug it in to the l2 node's ethernet
port. (Be sure to use the PoE injector or else the AP will not power up.)

In `/etc/network/interfaces.d/` create and edit `eth0` configuration file

```
allow-hotplug eth0 
iface eth0 inet static
  address 10.100.1.10
  netmask 255.255.255.0
  gateway 10.100.1.1
```
##### eth1
TBD

##### wlan0
TBD

##### Peering with the Local Mesh with wlan1
This interface will be configured for the BATMAN-Adv l2 routing system. 


### Interface configurations
We start by configuring wlan0 to load in IBSS mode

>Depending on your WNIC, you may need to modify the config below to better suit
your configuration. Our system we're configuring has an PCI-E WNIC installed
along with a physical ethernet card on the mainboard. 

wlan0 will be used to peer with the mesh network. In `/etc/network/interfaces.d/` 
create and edit `wlan0' configuration file:
```
auto wlan0
iface wlan0 inet manual
  mtu 1532
  wireless-essid example-mesh #insert essid here
  wireless-channel 1 
  wireless-mode ad-hoc
```

Next, we'll configure the virtual interface bat0 with the a static IP:

In `/etc/network/interfaces.d/` create and edit `bat0' configuration file

```
auto bat0
iface bat0 inet static
  address 10.100.2.1
  netmask 255.255.255.0
  gateway 10.100.1.1
  pre-up batctl if add wlan0
  post-up batctl gw_mode server
  down batctl if del wlan0
```

###### Prevent `dhcpcd` from configuring wlan0
**NOTE:** If DHCPCD is installed, the interfaces we're toying around with need
to be denied from DHCPCD's control. 

In `/etc/dhcpcd.conf/`, add the following statement on the bottom of the document.

```
denyinterfaces wlan0, bat0
```

#### Enabling IPv4 forwarding
To pass traffic not addressed to the host between interfaces, ip_forwarding must
be enabled. In Linux, this setting is applied at runtime. The following command
enables forwarding immediately.
```
echo 1 > /proc/sys/net/ipv4/ip_forward
```
To make this configuration persist across reboots, open /etc/sysctl.conf and 
uncomment line 28
```
net.ipv4.ip_forward=1
```
### OSPF
L2 nodes bridge Local Mesh Networks (LMN) with donated backhaul which eventually
reaches the Internet. As of right now, OSPF has been selected to maintain
routing tables across the mesh network. 

> In the future, OSPF, IPv4, and other conventional protocols may not be in use
> for mesh-level communications since these protocols are designed to be used
> within an centralized autonomous system, such as an enterprise LAN.

Configuration is completed using the frrouting suite which has already been 
installed (See "Update and Install necessary software").

According to the OSPF section in the network map, we'll configure this carrier
for the west lab subnet. See the table below. 

###### OSPF Configuration Table
| Parameter  | Value  
|------------|-----------
| Area ID    | west_lab 
| AreaNumber | 10.11.1.0
| Range      | 10.11.1.0/24 
| type       | nssa  

##### OSPF Configuration in Linux
First, we need to set frrouting to run ospfd. Open `/etc/frr/daemons` and change 
`ospfd=no` to `ospfd=yes`. Since we're running the daemon on this host, not much
else needs to be configured. 

To access the configuration shell, use the following command as user `root`.
```
vtysh
```
Enter configuration mode.
```
host> config t
```
Generate a new ospf instance.
```
host(config)> router ospf
```
You'll see the prompt updates, the following will build the configuration
```
host(config-ospf)> area 10.11.1.0 nssa 
host(config-ospf)> network 10.11.1.0/24 area 10.11.1.0
host(config-ospf)> redistribute kernel
```
- `area 10.11.1.0 nssa` sets the area up as a not-so-stubby-area since we're
  going to forward routes on bat0 to the rest of the backbone. 
- `network ...` Tells OSPF what network to establish adjacencies.
- `redistribute kernel` Allows routes created under the kernel scope to be
  redistributed across the OSPF domain. 

Finally, end the configuration session and write the new settings to disk. 
```
host(config-ospf)> exit
write
```
