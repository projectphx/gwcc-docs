# Configure a L2 node on Raspbian  

> *Note:*
> Your mileage may very depending on the host's OS. Currently, Raspbian is in use
> for testing so this guide will assume the reader is either running a Raspbian
> instance or knows how to translate instructions over to their respective
> platform(s). 

## Overview

This guide will go over configuring a private wireless network on a L3 node. 

## Installation

```
$ sudo aptitude update && sudo aptitude upgrade
$ sudo aptitude install -y hostapd dnsmasq bridge-utils batctl
```
Install frrouting. For Raspbian based systems, visit https://deb.frrouting.org/
for instructions to get the software. 

## Configuration

#### Interface level configurations

To pain a more clear picture, take a moment to imagine that you just registered
to run a L2 node. The network administrator has allocated you the following:
  - An IP for your Nanostation AP and your l2 node (In reality, a DHCP
    reservation would be configured, but during this experimental setup we've
    chosen to go with static IPs).
  - An OSPF Configuration (Area number to join)
  - And a block of IPs that will be issued to L3 nodes in your area. 

| Assignment      | Value           |
|-----------------|-----------------|
| NSM5            | 10.100.1.11/24  |
| L2 Node         | 10.100.1.10/24  |
| Default Gateway | 10.100.1.1      |
| OSPF Area       | 0               |
| IP Block        | 10.100.2.0/24   |

###### eth0
Set up the AP with the assigned IP and then plug it in to the l2 node's ethernet
port. (Be sure to use the PoE injector or else the AP will not power up.)

In `/etc/network/interfaces.d/` create and edit `eth0` configuration file

```
allow-hotplug eth0 
iface eth0 inet static
  address 10.11.1.4
  netmask 255.255.255.0
  gateway 10.11.1.1
```
##### eth1
TBD

##### wlan0
TBD

##### wlan1
This interface will be configured for the BATMAN-Adv l2 routing system. 

###### Identify Raspberry Pi WNIC

For batman-adv, we'll be using a USB external wireless dongle to peer with the
IBSS. To determine which interface is not the built-in WNIC included on the
Raspberry pi, the following OUI's are available to reference.

By default, the Raspberry pi foundation's OUI numbers are as follows:
```
B8-27-EB   (hex)            Raspberry Pi Foundation
B827EB     (base 16)        Raspberry Pi Foundation
			    Mitchell Wood House
			    Caldecote  Cambridgeshire  CB23 7NU
			    US

DC-A6-32   (hex)            Raspberry Pi Trading Ltd
DCA632     (base 16)        Raspberry Pi Trading Ltd
                            Maurice Wilkes Building, Cowley Road
			    Cambridge    CB4 0DS
			    GB
```
[Check current OUI status][2]

###### Interface configurations
We start by configuring wlan0 to load in IBSS mode

In `/etc/network/interfaces.d/` create and edit `wlan0' configuration file

```
auto wlan0
iface wlan0 inet manual
  mtu 1532
  wireless-channel 1
  wireless-mode ad-hoc
```

Next, we'll configure the virtual interface bat0 with the a static IP:

In `/etc/network/interfaces.d/` create and edit `bat0' configuration file

```
auto bat0
iface bat0 inet static
  address 10.100.1.1
  netmask 255.255.255.0
  gateway 10.100.1.1
  up batctl if add wlan0
  post-up batctl gw_mode server
  down batctl if del wlan0
```

###### Prevent `dhcpcd` from configuring wlan0

In `/etc/dhcpcd.conf/`, add the following statement on the bottom of the document.

```
denyinterfaces wlan0
```
Otherwise, you can also just disable this daemon entirely
```
systemctl disable dhcpcd.service
systemctl stop dhcpcd.service
```

#### Enabling IPv4 forwarding
To pass traffic not addressed to the host between interfaces, ip_forwarding must
be enabled. To do this, open /etc/sysctl.conf and uncomment line 28
```
net.ipv4.ip_forward=1
```
### OSPF
L2 nodes bridge Local Mesh Networks (LMN) with donated backhaul which eventually
reaches the Internet. As of right now, OSPF has been selected to maintain
routing tables across the mesh network. 

> In the future, OSPF, IPv4, and other conventional protocols may not be in use
> for mesh-level communications since these protocols are designed to be used
> within an centralized autonomous system, such as an enterprise LAN.

Configuration is completed using the frrouting suite which has already been 
installed (See "Update and Install necessary software").

According to the OSPF section in the network map, we'll configure this carrier
for the west lab subnet. See the table below. 

###### OSPF Configuration Table
| Parameter  | Value  
|------------|-----------
| Area ID    | west_lab 
| AreaNumber | 10.11.1.0
| Range      | 10.11.1.0/24 
| type       | nssa  

##### OSPF Configuration in Linux
First, we need to set frrouting to run ospfd. Open `/etc/frr/daemons` and change 
`ospfd=no` to `ospfd=yes`. Since we're running the daemon on this host, not much
else needs to be configured. 

To access the configuration shell, use the following command as user `root`.
```
vtysh
```
Enter configuration mode.
```
host> config t
```
Generate a new ospf instance.
```
host(config)> router ospf
```
You'll see the prompt updates, the following will build the configuration
```
host(config-ospf)> area 10.11.1.0 nssa 
host(config-ospf)> network 10.11.1.0/24 area 10.11.1.0
host(config-ospf)> redistribute kernel
```
- `area 10.11.1.0 nssa` sets the area up as a not-so-stubby-area since we're
  going to forward routes on bat0 to the rest of the backbone. 
- `network ...` Tells OSPF what network to establish adjacencies.
- `redistribute kernel` Allows routes created under the kernel scope to be
  redistributed across the OSPF domain. 

Finally, end the configuration session and write the new settings to disk. 
```
host(config-ospf)> exit
write
```
