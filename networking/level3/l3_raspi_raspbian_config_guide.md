# Configure a L3 node + AP

*Note:*

Your mileage may very depending on the host's OS. Currently, Raspbian is in use
for testing so this guide will assume the reader is either running a Raspbian
instance or knows how to translate instructions over to their respective
platform(s). 

## Overview

This guide will go over configuring a private wireless network on a L3 node. 

## Installation

```
$ sudo aptitude update && sudo aptitude upgrade
$ sudo aptitude install -y batctl hostapd dnsmasq bridge-utils
```
- batctl: B.A.T.M.A.N-Adv utility.
- hostapd: 802.11 WLAN authentication management
- dnsmasq: DNS & DHCP server designed for easy and quick configuration
- bridge-utils: L2 bridge configuration utilities

## Configuration

In Debian Stretch (Specifically the Raspbian variation), editing
`/etc/network/interfaces` directly instead of placing each interface inside it's
own configuration file under `/etc/network/interfaces.d/` causes problems during
automatic network initialization. Because of this, it's recommended that static
interface configurations are set up in `/etc/network/interfaces.d/` directory during
testing. Eventually, configurations should be made somewhere else (such as
systemd network/link/service configuration files.) 


#### Private Wireless Network Configurations

Our private network will use the Class C private IP addressing scheme
192.168.4.0/24. 

In `/etc/network/interfaces.d/` create and edit a file named `wlan0`

/etc/network/interfaces.d/wlan0

```
auto wlan0
iface wlan0 inet static
  address 192.168.4.1
  netmask 255.255.255.0
  #Leave gateway blank for future BATMAN interface address
  #gateway
```

###### Prevent `dhcpcd` from configuring wlan0

In `/etc/dhcpcd.conf/`, add the following statement on the bottom of the document.
```
denyinterfaces wlan0
```

#### DHCP and local DNS
Disable systemd-networkd binding to port 53.
Edit /etc/systemd/networkd.conf by

- Uncomment Line 19 `domain-needed`
- Uncomment line 21 `bogus-priv`
- Uncomment and edit line 107:
  - set `interface` to `interface=wlan0` (or br0)
- Uncomment 136
- Uncomment and modify 145 to `domain=home.local`
- Uncomment 158

#### NAT
iptables will be used to set up IP masquerading

copy or download the iptables script to place in ~/
```
#!/bin/bash
sudo iptables -A INPUT -4 -p tcp -m state --state ESTABLISHED,RELATED -J ACCEPT
sudo iptables -A INPUT -4 -p udp -m state --state ESTABLISHED,RELATED -J ACCEPT
sudo iptables -t nat -A POSTROUTING -o bat0 -j MASQUERADE
```
Ensure the execute bit is set for the script. Don't forget to set the owner to root! 
```
chmod +x /sbin/apply-iptables-rules.sh && chown root:root /sbin/apply-iptables-rules.sh
```
Create a symbolic link from /sbin 
```
sudo ln -s apply-iptables-rules.sh /sbin/apply-iptables-rules.sh
```
 
Finally, configure ifconfig to apply these rules when wlan0 comes up at boot time

in `/etc/network/interfaces.d/02-wlan0` append the following:
```
up /sbin/apply-iptables-rules.sh
```

#### Enabling IPv4 forwarding
To pass traffic not addressed to the host between interfaces, ip_forwarding must
be enabled. To do this, open /etc/sysctl.conf and uncomment line 28
```
net.ipv4.ip_forward=1
```
#### Network Authentication

To begin, we will configure hostapd, which is an IEEE 802.11 AP & IEEE
802.1x/WPA/WPA2/EAP/RADIUS authentication solution. 

The Raspberry pi 3B and B+ come with an 802.11 NIC, which can be identified
and configured to run in AP mode. 

##### Identify Raspberry Pi WNIC

By default, the Raspberry pi foundation's OUI numbers are as follows:
```
B8-27-EB   (hex)            Raspberry Pi Foundation
B827EB     (base 16)        Raspberry Pi Foundation
			    Mitchell Wood House
			    Caldecote  Cambridgeshire  CB23 7NU
			    US

DC-A6-32   (hex)            Raspberry Pi Trading Ltd
DCA632     (base 16)        Raspberry Pi Trading Ltd
                            Maurice Wilkes Building, Cowley Road
			    Cambridge    CB4 0DS
			    GB
```
[Check current OUI status][2]

Create a new `hostapd.conf` file in */etc/* and insert the following:

```
interface=wlan0
driver=nl80211
ssid=ProjectPhoenix
hw_mode=g
channel=6
cl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=JoinProjPhx
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```
See more information on [`hostapd`][1].

Finally, edit /etc/network/interfaces.d/02-wlan0 and add a statement to launch
hostapd

```
auto wlan0
iface wlan0 inet static
  address 192.168.4.1
  netmask 255.255.255.0
  hostapd /etc/hostapd.conf
```

#### Configure BATMAN Interface

First, we configure our USB wireless interface to be brought up as an ad-hoc
interface at boot.

In `/etc/network/interfaces.d/` create and edit `wlan1` configuration file and
enter the following:

```
auto wlan1
iface wlan1 inet manual
  wireless-mode ad-hoc
  wireless-channel 1
  wireless-essid mesh
  mtu 1532
```
This configuration configures our wireless interface to run in ad-hoc mode on
2412MHz (2.4GHz - Channel 1). The SSID for the network is 'mesh' and the MTU has
been expanded from the standard 1500 to 1532 bytes so that the BATMAN-ADV header
can be prepended to the 802.11 frame to allow normal sized frames to pass
through the network. 

Next, we configure `bat0`. This interface is a virtual-interface created when
the batman-adv kernel module is configured to send/receive data over a physical
WNIC such as the previously configured `wlan1`. 

In `/etc/network/interfaces.d/` create and edit `bat0` configuration file and
enter the following:
```
auto bat0
iface bat0 inet dhcp
  pre-up /usr/sbin/batctl if add wlan1
  post-up /usr/sbin/batctl gw_mode client
  pre-down batctl if del wlan1
```

[1]: https://wireless.wiki.kernel.org/en/users/documentation/hostapd
[2]: http://standards-oui.ieee.org/oui.txt

