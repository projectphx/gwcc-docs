# Accessing Factory Fresh Routers

Use this table to access factory fresh / factory reset routers, switches, and
APs. 

## Single Board Computers
- Raspberry Pi (Raspbian 9 Stretch)
  + Username: `pi`
  + Password: `Raspberry`

## Access Points
### Ubituiti
#### Nanostation M5
  
- Default Network:
  + IP: 192.168.1.20
  + Netmask: 255.255.255.0

- Username: ubnt
- Password: ubnt

## Routers
### Mikrotik
#### Hex PoE Lite

- Default Network:
  + IP: 192.168.88.1
  + Netmask: 255.255.255.0

- Username: admin
- Password: <blank>
