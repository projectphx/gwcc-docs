# Configuring a raspberry pi.
These configuration steps can be performed on a Linux/Unix system, such as
Ubuntu, Fedora, or MacOS. 

The following sections pertain to configuring a Raspberry pi for deployment as
either a _Mesh Client (l3)_ or a _Mesh Carrier (l2)_ (See the Master manual page for
definitions). 

## Initial host configuration

### Hardware Requirements
The following list describes the hardware requirements for this specific setup.
Feel free to generate a pull request if you've tested other equipment that can
work. 

- Raspberry pi 3 Model B or B+
- MicroUSB 2.1A Power Supply. 
  *Note:* The Raspberry Pi 3 Model B+ Requires a constant 2.1A Power supply in
  order to boot. Verify proper power requirements are fulfilled. 
- 16GB MicroSD card or greater. MicroSD to SD card adapter might be necessary. 
- N-WiFi (Find details)
- HDMI cable, Monitor with appropriate HDMI support, Keyboard & Mouse.
- Some form of insulative case to protect delicate components on the circuit
  board from damage & shorting. 
- Ubiquiti AirMax capable wireless networking hardware
- CAT6 or greater UTP Ethernet cable. 

### Installing Raspbian
Raspbian has been selected for use on this project since it's maintained by the
same vendors who have created the Raspberry Pi. We will be using the *lite*
version since we do not need any desktop software on the node. 

Download the OS from the Raspberry Pi Foundation's website. Be sure to scroll
down and select 'Raspbian Stretch Lite':

- https://www.raspberrypi.org/downloads/raspbian/

Once the image is downloaded, open a terminal and navigate to the directory that
the image was saved to. 

From there, plug in the MicroSD Card to your computer and verify the device name
for the disk. On Linux systems, you can use `lsblk`, on MacOS you can use
`diskutil list`. 

##### Identify SDCard device name on Ubuntu
```
$ lsblk
... Ouput Tuncated ...

sda      8:0    0    20G  0 disk 
└─sda1   8:1    0    20G  0 part /
sdb      9:0    0    16G  0 disk 
└─sdb1   9:1    0    16G  0 part /media/sdcard

... Ouput Tuncated ...
```
From the output of `lsblk`, `sdb` is the device name of our SDCard.

##### Identify SDcard device name on MacOS
```
$ diskutil list

... Ouput Tuncated ...

/dev/disk4 (internal, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:     FDisk_partition_scheme                        *16.0 GB    disk4
   1:             Windows_FAT_32 SDcard                  15.5 GB    disk4s1

... Ouput Tuncated ...
```
Likewise, we see that `/dev/disk4` is our SDcard. 

*NOTE:* You are responsible for verifying and ensuring the disk is correct and
all sensitive data is backed up before moving forward. I am not responsible for
any damage to data if you do this incorrectly. 

#### Write Raspbian to SDCard

Now that we've set our current working directory and verified the name of our
target device. We can go ahead and write the image to the SDcard. In the
terminal, use `dd` to perform the task. 

##### Using `dd`
```
$ sudo dd if=./2018-06-27-raspbian-stretch-lite.img of=/dev/disk4
```

... Some time passes, once complete you should see the if the job
completes successfully.  

```
3637248+0 records in
3637248+0 records out
1862270976 bytes transferred in 716.874408 secs (2597765 bytes/sec)
```

#### First Boot

Insert the newly-formatted SDcard into the Raspberry Pi and plug in the
necessary peripherals. Once the system is booted into Raspbian, you should
see a login shell. Use the following default credentials to authenticate. 

```
Raspian Default Login Credential
================================
Username: pi
Password: Raspberry
```

#### Boilerplate configurations

Once logged in, load up `raspi-config` for setting basic system configurations. 

```
sudo raspi-config
```

*NOTE:* `sudo` does not require the user `pi` to enter a password.

Run through the configuration wizard to set up essential runtime configurations
for the system. 

- Change password
- Enabling ssh for remote access will ease configurations if you plug the device
  into your LAN.
- Resize the filesystem to utilize the entire capacity of the SD card.
- Connect to Wifi
- Configure Locale setting / keyboard layout. 

From there, run updates.
```
sudo apt update && sudo apt -y upgrade
```
