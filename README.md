Project Phoenix @ Gateway Community College
===========================================
## Overview 

![Network Architecture Overview Graphic][5]

Mesh-clients (classified under level 3) peer with a local IBSS (Wireless ad-hoc
network) which run a specialized L2 routing protocol called B.A.T.M.A.N-Adv in
between the node's link layer and network layer. From the point of view from the
client, all peers can communicate with each other on the same broadcast domain
similar to a L2 switch. 

Each client is configured to pass traffic from their private network interfaces
to the mesh network utilizing NAT. Each host's private network interface is
configured to serve as a simple 802.11bgn WLAN which provides network access to
a private user's personal devices. 

Each Local Mesh Network has at least one l2 node connected to the network which
functions as a distribution node within the network. Traffic is routed between 
the access layer and the core layer of the network. These nodes also handle
issuing DHCP leases to clients and maintaining routing tables using OSPF. 

Traffic addressed to the Internet or other parts of the mesh network pass
through core nodes (level 3) which have full view of all available routes on the
network as well as a dedicated internet connection as well as other services
such as NAT and ensuring network security. 

This repository contains documentation for an early conceptual mesh network
implementation at Gateway Community College.

**Note:** This project is under active development. The current repository
structure is subject to change frequently. In the future, a dedicated
docuementation site will be set up to host this content to provide a much better
experience for readers. 

## Contents 
- [Network Architecture Overview][1]
- Hardware
- Networking
  - [Level 1 Configuration][2]
  - [Level 2 Configuration][3]
  - [Level 3 Configuration][4]
- demo_software

## How to view network maps
Currently network maps are created in [draw.io][6]. Sources are saved under the
`.drawio` file extension and can viewed on the webservice. 

## `demo_software` directory structure TBD pending commit from team member

[1]: overview.md
[2]: networking/level1/l1_mikrotik_hexpoelite_config_guide.md
[3]: networking/level2/l2_raspi_raspbian_config_guide.md
[4]: networking/level3/l3_raspi_raspbian_config_guide.md
[5]: media/netarch_overview.png
[6]: https://draw.io

